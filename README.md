# Centbee Java image

### Pull newest build from Docker Hub
```
docker pull centbee/java:latest
```

### Run image
```
docker run -it centbee/java:latest bash
```

### Use as base image
```Dockerfile
FROM centbee/java:latest
```
